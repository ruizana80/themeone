import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    testimonial: [
      {
        title: "Paula",
        subTitle: "17 años",
        image: require("../assets/testimonials/testimonials-1.jpg"),
        text:
          "Gracias por ayudarme a sanar mis heridas, por enseñarme que la principal prioridad soy yo; por escuchar “un millón de veces” las mismas historias, por ser parte de mi vida, por que gracias a ti y a tu excelente trabajo, hoy soy lo que soy; y sé que aún me falta “muchísimo” por recorrer, de verdad no tengo palabras para agradecerte todo. "
      },
      {
        title: "María",
        subTitle: "22 años",
        image: require("../assets/testimonials/testimonials-2.jpg"),
        text:
          "Al inicio debo decir que no sabia si funcionaria la terapia, pero ahora como te lo he platicado, estoy asombrada de cómo ha sido mi cura, y en verdad te agradezco mucho por ayudarme. Tienes ese poder mágico para ayudarnos a sanar. Muchas Gracias."
      },
      {
        title: "Familia Costa",
        subTitle: "",
        image: require("../assets/testimonials/testimonials-3.jpg"),
        text:
          "Hemos tenido un gran avance positivo, la relación de madre a hija ha mejorado, gracias por toda la paciencia y por escucharnos."
      }
      // {
      //   title: "Matt Brandon",
      //   subTitle: "Freelancer",
      //   image: require("../assets/testimonials/testimonials-4.jpg"),
      //   text:
      //     "Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam."
      // },
      // {
      //   title: "John Larson",
      //   subTitle: "Entrepreneur",
      //   image: require("../assets/testimonials/testimonials-5.jpg"),
      //   text:
      //     "Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid."
      // }
    ]
  },
  getters: {
    cartItems: state => {
      return state.cartItems;
    }
  },
  mutations: {
    cart(state, payload) {
      payload.forEach(element => {
        state.cartItems.push(element);
      });
    }
  },
  actions: {
    cart({ commit }, payload) {
      commit("cart", payload);
    }
  },
  modules: {}
});
