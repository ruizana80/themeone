import Vue from "vue";
import BootstrapVue from "bootstrap-vue/dist/bootstrap-vue.esm";
import "./assets/style/style.scss";
import "./assets/icofont/icofont.min.css";

import AOS from "aos";
import "aos/dist/aos.css";

import App from "./App.vue";
import router from "./router";
import store from "./store";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import Lightbox from "vue-my-photos";
Vue.component("lightbox", Lightbox);
Vue.use(Lightbox);

Vue.use(BootstrapVue);
Vue.config.productionTip = false;
Vue.use(Lightbox);

new Vue({
  created() {
    AOS.init();
  },
  router,
  store: store,
  render: h => h(App)
}).$mount("#app");
